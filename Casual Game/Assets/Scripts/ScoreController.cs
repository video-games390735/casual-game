﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    public Text scoreText;
    public Text highScore;

    private float playerScore;
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GameObject.Find("Player").GetComponent<Rigidbody2D>();
        highScore.text = "High Score: " + Mathf.Round(PlayerPrefs.GetFloat("HighScore: ", 0));
    }

    private void Update()
    {
        if (rb.velocity.y > 0 && transform.position.y > playerScore) playerScore = transform.position.y;

        scoreText.text = "Score: " + Mathf.Round(playerScore);

        if (playerScore > Mathf.Round(PlayerPrefs.GetFloat("HighScore: ", 0))) PlayerPrefs.SetFloat("HighScore: ", playerScore);
    }
}