﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    public float playerSpeed = 10f;

    private float moveInput;
    private Rigidbody2D playerRigidBody;
    private float xPos;

    private void Start()
    {
        playerRigidBody = GetComponent<Rigidbody2D>();
    }

    public void Update()
    {
        moveInput = Input.GetAxis("Horizontal");
        playerRigidBody.velocity = new Vector2(moveInput * playerSpeed, playerRigidBody.velocity.y);

        if (transform.position.x < -8.8 || transform.position.x > 8.8)
        {
            Vector3 position = transform.position;
            xPos = Mathf.Clamp(position.x, 8.8f, -8.8f);
            position = new Vector2(xPos, position.y);
            transform.position = position;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            SceneManager.LoadScene("Game Over");
        }
    }
}