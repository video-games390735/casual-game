﻿using UnityEngine;

public class PlatformController : MonoBehaviour
{
    public float jumpForce = 25f;
    public float tileSpeed = 7;
    private AudioSource boing;
    
    private bool moveRight = true;

    private void Start()
    {
        boing = GetComponent<AudioSource>();
    }
    private void Update()
    {
        if (transform.position.x > 6.6f)
            moveRight = false;

        if (transform.position.x < -6.6f)
            moveRight = true;

        if (moveRight)
            transform.position = new Vector2(transform.position.x + tileSpeed * Time.deltaTime, transform.position.y);
        else
            transform.position = new Vector2(transform.position.x - tileSpeed * Time.deltaTime, transform.position.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.y <= 0)
        {
            Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                Vector2 velocity = rb.velocity;
                velocity.y = jumpForce;
                rb.velocity = velocity;
            }
            boing.Play();
        }
    }
}