<h1 align="center">Download Link</h1>

<p align="center">https://alexandru-tataru.itch.io/casual-game</p>

<h1 align="center">DESCRIPTION</h1>
<p align="center">This is my first solo project that I've made in order to learn the basics of programming and Unity. It's a simple 2D game with the goal to achieve the highest score possible while avoiding enemies and jumping up on platforms.

- Created the character controller and platform power up scripts. 
- Made a script that endlessly generates platforms in random positions that the player can jump on until he falls off or gets hit by an enemy.
- Wrote a script that keeps track of the player score based upon the height he reached and highest score reached.
- Drawn all the 2D game assets in Photoshop using a graphic tablet.

 </p>
<h1 align="center">BUILT WITH</h1>
<p align="center">Rider(IDE), GitLab, Unity Engine, C#, Photoshop</p>

<h1 align="center">SCREENSHOTS</h1>
<img src="https://i.imgur.com/PBsEiPQ.png" align="center">
<img src="https://i.imgur.com/8quFXoH.png" align="center">
<img src="https://i.imgur.com/d1WKlBr.png" align="center">
<img src="https://i.imgur.com/OUvgJOg.png" align="center">


<h1 align="center">OBSERVATIONS</h1>
<p align="center">The code has no comments at the moment</p>
